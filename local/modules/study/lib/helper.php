<?php

namespace Bitrix\Study;

use Bitrix\Main\Config\Option;
use CEvent;
use CIBlock;
use CIBlockElement;

class Helper
{
    public static function getIblock(int $id = null): array
    {
        $filter = ["ACTIVE" => "Y"];
        if ($id) {
            $filter = array_merge(
                $filter,
                ["ID" => $id]
            );
        }
        $rs = CIBlock::GetList(
            ["ID" => "ASC"],
            $filter
        );

        $result = [];
        while ($item = $rs->GetNext()) {
            $result[$item["ID"]] = [
                "NAME" => $item["NAME"],
                "CODE" => $item["CODE"],
            ];
        }

        if ($id && $result) {
            return $result[$id];
        } else {
            return $result;
        }
    }

    static function OnAfterIBlockElementAddHandler($arFields): void
    {
        if (self::isEqualIblock($arFields)) {
            self::saveStudyHl($arFields);
            if ($email = Option::get("study", "company_email")) {
                $iblock = self::getIblock($arFields["IBLOCK_ID"]);
                CEvent::Send(
                    "STUDY_MODULE_EVENT",
                    "s1",
                    [
                        "IBLOCK_NAME" => $iblock["NAME"],
                        "NAME" => $arFields["NAME"],
                        "PREVIEW_TEXT" => $arFields["PREVIEW_TEXT"],
                        "EMAIL_TO" => $email,
                    ]
                );
            }
        }
    }

    static function OnBeforeIBlockElementUpdateHandler($arFields): void
    {
        if (self::isEqualIblock($arFields)) {
            $currentElem = self::getCurrentElement($arFields["ID"], ["NAME", "PREVIEW_TEXT"]);
            if ($currentElem["NAME"] !== $arFields["NAME"] || $currentElem["PREVIEW_TEXT"] !== $arFields["PREVIEW_TEXT"]) {
                self::saveStudyHl($arFields);
            }
        }
    }

    private static function isEqualIblock(array $arFields): bool
    {
        return self::getIblock($arFields["IBLOCK_ID"])["CODE"] === Option::get("study", "company_iblock");
    }

    private static function saveStudyHl(array $arFields): void
    {
        global $USER;
        StudyHlblockTable::add([
            "fields" => [
                "UF_STUDY_USER_ID" => $USER->GetID() ?: "",
                "UF_STUDY_ELEMENT_ID" => $arFields["ID"],
                "UF_STUDY_ELEMENT_NAME" => $arFields["NAME"],
                "UF_STUDY_PREVIEW_TEXT" => $arFields["PREVIEW_TEXT"],
            ]
        ]);
    }

    private static function getCurrentElement(int $id, array $arSelect): array
    {
        return CIBlockElement::GetList(
            [],
            ["ID" => $id],
            false,
            false,
            $arSelect,
        )->GetNext() ?? [];
    }
}

