<?php
namespace Bitrix\Study;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class StudyTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_STUDY_CREATED_AT datetime optional
 * <li> UF_STUDY_USER_ID int optional
 * <li> UF_STUDY_ELEMENT_ID int optional
 * <li> UF_STUDY_ELEMENT_NAME text optional
 * <li> UF_STUDY_PREVIEW_TEXT text optional
 * </ul>
 *
 * @package Bitrix\First
 **/

class StudyHlblockTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'hl_first_study';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('STUDY_ENTITY_ID_FIELD')
                ]
            ),
            new DatetimeField(
                'UF_STUDY_CREATED_AT',
                [
                    'title' => Loc::getMessage('STUDY_ENTITY_UF_STUDY_CREATED_AT_FIELD')
                ]
            ),
            new IntegerField(
                'UF_STUDY_USER_ID',
                [
                    'title' => Loc::getMessage('STUDY_ENTITY_UF_STUDY_USER_ID_FIELD')
                ]
            ),
            new IntegerField(
                'UF_STUDY_ELEMENT_ID',
                [
                    'title' => Loc::getMessage('STUDY_ENTITY_UF_STUDY_ELEMENT_ID_FIELD')
                ]
            ),
            new TextField(
                'UF_STUDY_ELEMENT_NAME',
                [
                    'title' => Loc::getMessage('STUDY_ENTITY_UF_STUDY_ELEMENT_NAME_FIELD')
                ]
            ),
            new TextField(
                'UF_STUDY_PREVIEW_TEXT',
                [
                    'title' => Loc::getMessage('STUDY_ENTITY_UF_STUDY_PREVIEW_TEXT_FIELD')
                ]
            ),
        ];
    }
}