<?php

$mailEventTypes = [
    "STUDY_MODULE_EVENT" => [
        "EVENT_NAME"    => "STUDY_MODULE_EVENT",
        "NAME"          => "Добавление элемента в инфоблок, указанный в учебном модуле",
        "LID"           => "ru",
        "DESCRIPTION"   => "
                   #BLOCK_NAME# - Наименование инфоблока
                   #NAME# - Наименование элемента
                   #PREVIEW_TEXT# - Анонс
                   #EMAIL_TO# - Email-адреса, указанные в настройках учебного модуля  
               ",
    ],
];

$mailEventTempplates = [
    [
        "ACTIVE" => "Y",
        "EVENT_NAME" => "STUDY_MODULE_EVENT",
        "LID" => ["s1"],
        "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
        "EMAIL_TO" => "#EMAIL_TO#",
        "SUBJECT" => "Сообщение от учебного модуля",
        "BODY_TYPE" => "text",
        "MESSAGE" => "
                    В инфоблок #BLOCK_NAME# добавлен новый элемент:\n
                    Наименование: \"#NAME#\"\n
                    Анонс: \"#PREVIEW_TEXT#\"
                ",
    ],
];
