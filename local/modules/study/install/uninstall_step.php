<?php
if (! check_bitrix_sessid()) {
    return;
}

/** @var $APPLICATION */
?>
<form action="<?=$APPLICATION->GetCurPage()?>">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="id" value="study">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="uninstall_step" value="Y">
    <?CAdminMessage::ShowMessage("Внимание! Модуль будет удален из системы.")?>
    <p>Вы можете сохранить данные в таблице базы данных:</p>
    <p>
        <label for="saveHl">Сохранить таблицу</label>
        <input type="checkbox" name="saveHl" id="saveHl" value="Y" checked>
    </p>
    <input type="submit" name="" value="Удалить модуль">
</form>