<?php

use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Class study extends CModule
{
    const HL_PATH = __DIR__ . "/highload_block/";
    function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");
        $this->MODULE_ID = 'study';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "Учебный модуль";
        $this->MODULE_DESCRIPTION = "Учебный модуль";
//      $this->PARTNER_NAME = "Study partner - study author";
//      $this->PARTNER_URI = "http://www.studymodule.ru";
    }

    function DoInstall(): void
    {
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences(
            "iblock",
            "OnAfterIBlockElementAdd",
            "{$this->MODULE_ID}",
            "\\Bitrix\\Study\\Helper", "OnAfterIBlockElementAddHandler"
        );
        RegisterModuleDependences(
            "iblock",
            "OnBeforeIBlockElementUpdate",
            "{$this->MODULE_ID}",
            "\\Bitrix\\Study\\Helper", "OnBeforeIBlockElementUpdateHandler"
        );
        $this->InstallMailEvent();
        $res = $this->AddHlBlock();
        AddMessage2Log($res);
    }

    function DoUninstall(): void
    {
        global $APPLICATION, $DOCUMENT_ROOT, $uninstall_step;
        if ($uninstall_step) {
            Option::delete($this->MODULE_ID);
            UnRegisterModule($this->MODULE_ID);
            UnRegisterModuleDependences(
                "iblock",
                "OnAfterIBlockElementAdd",
                "{$this->MODULE_ID}",
                "\\Bitrix\\Study\\Helper", "OnAfterIBlockElementAddHandler"
            );
            UnRegisterModuleDependences(
                "iblock",
                "OnBeforeIBlockElementUpdate",
                "{$this->MODULE_ID}",
                "\\Bitrix\\Study\\Helper", "OnBeforeIBlockElementUpdateHandler"
            );
            $this->UnInstallMailEvent();
            $this->DeleteHlBlock();
        } else {
            //Вывод формы для подтверждения удаления модуля
            $APPLICATION->IncludeAdminFile(
                "{$this->MODULE_NAME}",
                $DOCUMENT_ROOT."/local/modules/study/install/uninstall_step.php"
            );
        }
    }

    private function InstallMailEvent(): bool
    {
        $mailEventTypes = [];
        $mailEventTempplates = [];
        include(__DIR__ . "/mail_events/mailEventsDesc.php");

        foreach ($mailEventTypes as $mailEventType) {
            if (! (new CEventType())->Add($mailEventType)) {
                return false;
            };
        }

        foreach ($mailEventTempplates as $mailEventTempplate) {
            if (! (new CEventMessage())->Add($mailEventTempplate)) {
                return false;
            };
        }

        return true;
    }

    private function UnInstallMailEvent(): bool
    {
        $mailEventTypes = [];
        include(__DIR__ . "/mail_events/mailEventsDesc.php");

        if ($eventNames = array_keys($mailEventTypes)) {

            $rs = CEventMessage::GetList("", "", ["EVENT_NAME" => $eventNames]);

            while ($item = $rs->GetNext()) {
                if (! CEventMessage::Delete($item["ID"])) {
                    return false;
                }
            }

            foreach ($eventNames as $eventName) {
                if (! CEventType::Delete($eventName)) {
                    return false;
                }
            }
        }

        return true;
    }

    private function AddHlBlock(): bool
    {

        if (Option::get($this->MODULE_ID, "study_saveHl") === "Y") {
            Option::delete($this->MODULE_ID, ["name" => "study_saveHl"]);// - перенести в DoInstall после RegisterModule()
            return true;
        }

        $connection = Application::getInstance()->getConnection();

        try {
            if (! Loader::includeModule('highloadblock')) {
                return false;
            }

            $connection->startTransaction();

            foreach (scandir(self::HL_PATH) as $hlBlockDescription) {
                if (! strpos($hlBlockDescription, ".php")) {
                    continue;
                }

                $blockInfo = [];
                $blockLang = [];
                $fields = [];
                include($this::HL_PATH . $hlBlockDescription);

                $result = HighloadBlockTable::add($blockInfo);
                if (! $result->isSuccess()) {
                    throw new RuntimeException(implode("\n", $result->getErrorMessages()));
                }

                $hlBlockId = $result->getId();
                $result = HighloadBlockLangTable::add(['ID' => $hlBlockId] + $blockLang);
                if (! $result->isSuccess()) {
                    throw new RuntimeException(implode(PHP_EOL, $result->getErrorMessages()));
                }

                $entityManager = new CUserTypeEntity;

                foreach ($fields as $field) {
                    $isText = false;
                    if (strtolower($field['USER_TYPE_ID']) === 'text') {
                        $isText = true;
                        $field['USER_TYPE_ID'] = 'string';
                    }
                    $fieldId = $entityManager->Add(['ENTITY_ID' => "HLBLOCK_$hlBlockId"] + $field);
                    if (! $fieldId) {
                        throw new RuntimeException("Не удалось добавить поле $field[FIELD_NAME] в hl-блок {$blockInfo['NAME']}");
                    }
                    if ($isText) {
                        $connection->query("ALTER TABLE {$blockInfo['TABLE_NAME']} MODIFY {$field['FIELD_NAME']} text NOT NULL DEFAULT ''");
                    }
                }
            }

            $connection->commitTransaction();

        } catch (Throwable) {
            try {
                $connection->rollbackTransaction();
            } catch (Throwable) {}

            return false;
        }

        return true;
    }

    private function DeleteHlBlock(): bool
    {
        global $saveHl;

        if ($saveHl === "Y") {
            Option::set($this->MODULE_ID, "study_saveHl", "Y");// - перенести в DoUninstall
            return true;
        }

        $connection = Application::getInstance()->getConnection();

        try {
            if (! Loader::includeModule('highloadblock')) {
                return false;
            }

            $connection->startTransaction();

            foreach (scandir(self::HL_PATH) as $hlBlockDescription) {
                if (! strpos($hlBlockDescription, ".php")) {
                    continue;
                }

                $blockInfo = [];
                $blockLang = [];
                $fields = [];
                include($this::HL_PATH . $hlBlockDescription);

                $hlBlock = HighloadBlockTable::getRow(['filter' => ['=NAME' => $blockInfo['NAME']]]);
                if (! $hlBlock) {
                    throw new RuntimeException("Не найден hl-блок {$blockInfo['NAME']}");
                }

                $result = HighloadBlockTable::delete($hlBlock['ID']);
                if (! $result->isSuccess()) {
                    throw new RuntimeException(implode(PHP_EOL, $result->getErrorMessages()));
                }
            }

            $connection->commitTransaction();

        } catch (Throwable) {
            try {
                $connection->rollbackTransaction();
            } catch (Throwable) {}

            return false;
        }

        return true;
    }
}
