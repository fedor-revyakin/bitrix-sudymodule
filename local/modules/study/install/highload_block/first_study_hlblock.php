<?php

$blockInfo = [
    'NAME' => 'HlFirstStudy',
    'TABLE_NAME' => 'hl_first_study',
];

$blockLang = [
    'LID' => 'ru',
    'NAME' => 'HL-блок учебного модуля',
];

$fields = [
    [
        'FIELD_NAME' => 'UF_STUDY_CREATED_AT',
        'USER_TYPE_ID' => 'datetime',
        'XML_ID' => 'UF_STUDY_CREATED_AT',
        'SORT' => 100,
        'EDIT_FORM_LABEL' => ['ru' => 'Дата создания'],
        'LIST_COLUMN_LABEL' => ['ru' => 'Дата создания'],
        'LIST_FILTER_LABEL' => ['ru' => 'Дата создания'],
        'SETTINGS' => [
            'DEFAULT_VALUE' => [
                'TYPE' => 'NOW',
                'VALUE' => '',
            ],
            'USE_SECOND' => 'Y',
        ],
    ],
    [
        'FIELD_NAME' => 'UF_STUDY_USER_ID',
        'USER_TYPE_ID' => 'integer',
        'XML_ID' => 'UF_STUDY_USER_ID',
        'SORT' => 200,
        'EDIT_FORM_LABEL' => ['ru' => 'Идентификатор пользователя'],
        'LIST_COLUMN_LABEL' => ['ru' => 'Идентификатор пользователя'],
        'LIST_FILTER_LABEL' => ['ru' => 'Идентификатор пользователя'],
    ],
    [
        'FIELD_NAME' => 'UF_STUDY_ELEMENT_ID',
        'USER_TYPE_ID' => 'integer',
        'XML_ID' => 'UF_STUDY_ELEMENT_ID',
        'SORT' => 300,
        'EDIT_FORM_LABEL' => ['ru' => 'Идентификатор элемента'],
        'LIST_COLUMN_LABEL' => ['ru' => 'Идентификатор элемента'],
        'LIST_FILTER_LABEL' => ['ru' => 'Идентификатор элемента'],
    ],
    [
        'FIELD_NAME' => 'UF_STUDY_ELEMENT_NAME',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_STUDY_ELEMENT_NAME',
        'SORT' => 100,
        'EDIT_FORM_LABEL' => ['ru' => 'Наименование элемента'],
        'LIST_COLUMN_LABEL' => ['ru' => 'Наименование элемента'],
        'LIST_FILTER_LABEL' => ['ru' => 'Наименование элемента'],
    ],
    [
        'FIELD_NAME' => 'UF_STUDY_PREVIEW_TEXT',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_STUDY_PREVIEW_TEXT',
        'SORT' => 100,
        'EDIT_FORM_LABEL' => ['ru' => 'Текст анонса'],
        'LIST_COLUMN_LABEL' => ['ru' => 'Текст анонса'],
        'LIST_FILTER_LABEL' => ['ru' => 'Текст анонса'],
    ],
];
