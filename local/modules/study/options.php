<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Study\Helper;

/**
 * @var $APPLICATION CMain
 */
$module_id = "study";

Loader::includeModule($module_id);

$request = Context::getCurrent()->getRequest();

$iblocks = [];

foreach (Helper::getIblock() as $id => $iblock) {
    $iblocks[$iblock["CODE"]] = "[{$id}] - {$iblock["NAME"]}";
}

$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => "Настройки",
        "OPTIONS" => [
            [
                "company_name",
                "Наименование организации",
                "",
                ["text", 15],
            ],
            [
                "company_type",
                "Тип организации",
                "type_3",
                [
                    "selectbox",
                    [
                        "type_1" => "ОАО",
                        "type_2" => "ЗАО",
                        "type_3" => "ООО",
                        "type_4" => "ИП",
                    ]
                ],
            ],
            [
                "field_checkbox",
                "Использует УСН",
                "",
                ["checkbox"],
            ],
            [
                "company_iblock",
                "Список инфоблоков",
                "",
                [
                    "selectbox",
                    $iblocks,
                ],
            ],
            [
                "company_email",
                "Email адреса",
                "",
                ["textarea", 3, 15],
                "action" => "prepareTextarea",
            ],
        ]
    ]
];

if ($request->isPost() && $request["Update"] && check_bitrix_sessid()) {
    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            if (! is_array($arOption)) {
                continue;
            }

            if (is_array($arOption["note"])) {
                continue;
            }

            $optionName = $arOption[0];
            $optionValue = $request->getPost($arOption[0]);
            Option::set($module_id, $optionName, $optionValue);
        }
    }
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();?>

<form method="post"
      action="<?=$APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request["mid"])?>&amp;lang=<?=$request["lang"]?>"
      name="study_settings">

    <?php
        foreach ($aTabs as $aTab):
            if ($options = $aTab["OPTIONS"]):
                $tabControl->BeginNextTab();
                __AdmSettingsDrawList($module_id, $options);
            endif;
        endforeach;

        $tabControl->Buttons();
    ?>

    <input type="submit" name="Update" value="Сохранить">
    <input type="reset" name="reset" value="Отменить">

    <?=bitrix_sessid_post();?>
</form>
<?$tabControl->End();?>